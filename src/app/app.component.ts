import { Component, OnInit } from '@angular/core';
import { IEmployer } from "./components/form/interfaces";
import { CardsService } from "./components/card/cards.service";
import { LoaderService } from "./components/loader/loader.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  cards: IEmployer[]

  constructor(private service: CardsService, private loading: LoaderService) {
  }

  ngOnInit() {
    this.loading.open()
    this.service.load()
      .subscribe(employers => {
        this.cards = [...employers]
        this.loading.close()
      }, () => {
        this.loading.close()
      })
  }

  addEmployer(employer: IEmployer) {
    this.cards = [employer, ...this.cards]
  }

  removeEmployer(id: string) {
    this.cards = this.cards.filter(card => card.id !== id)
    this.loading.close()
  }
}
