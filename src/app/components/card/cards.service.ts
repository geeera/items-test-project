import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { IEmployer } from "../form/interfaces";
import { environment } from "../../../environments/environment";
import { map, toArray } from "rxjs/operators";
import { ValidationErrors } from "@angular/forms";

@Injectable({
  providedIn: 'root'
})

export class CardsService {

  constructor(private http: HttpClient) { }

  load():Observable<IEmployer[]> {
    return this.http.get<IEmployer[]>(`${environment.DB_URL}/users.json`)
      .pipe(
        map(emp => {
          const keys = Object.keys(emp)
          return keys.reduce((total: IEmployer[], amount: string) => {
            return [...total, { ...emp[amount], id: amount }]
          }, []).reverse()
        })
      )
  }

  create(employer: IEmployer): Observable<void> {
    return this.http.post<void>(`${environment.DB_URL}/users.json`, employer)
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>(`${environment.DB_URL}/users/${id}.json`)
  }

  validEmail(value: string): Observable<ValidationErrors> {
    const url = `${environment.DB_URL}/users.json?orderBy=\"email\"&equalTo=\"${value}\"&print=pretty`
    return this.http.get<IEmployer>(url)
      .pipe(
        map((res) => {
          const condition = Object.entries(res).length
          return !condition ? null : { emailIsExists: true }
        })
      )
  }
}
