export interface IFrameworks {
  name: string,
  versions: string[]
}

export interface IEmployer {
  id?: string,
  firstName: string
  lastName: string
  dateOfBirth: Date
  framework: string
  frameworkVersion: string
  email: string
  hobby: IHobby[]
}

export interface IHobby {
  name: string,
  duration: string
}
