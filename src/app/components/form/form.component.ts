import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from "@angular/material/form-field";
import { AsyncValidatorFn, FormArray, FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { IEmployer, IFrameworks } from "./interfaces";
import { CardsService } from "../card/cards.service";
import { EmailAsyncValidatorService } from "./email-async-validator.service";
import { LoaderService } from "../loader/loader.service";


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  providers: [
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {floatLabel: 'always'}}
  ]
})

export class FormComponent implements OnInit {
  @Output("addEmployer") addEmployer: EventEmitter<IEmployer> = new EventEmitter<IEmployer>()

  form: FormGroup
  frameworks: IFrameworks[] = [
    {name: 'angular', versions: ['1.1.1', '1.2.1', '1.3.3']},
    {name: 'react', versions: ['2.1.2', '3.2.4', '4.3.1']},
    {name: 'vue', versions: ['3.3.1', '5.2.1', '5.1.3']}
  ];

  get hobbies(): FormGroup[]  {
    const formArray: FormArray = <FormArray>this.form.get('hobby')
    return <FormGroup[]>formArray.controls;
  }

  get versions(): IFrameworks["versions"] {
    const framework: IFrameworks = this.form.get('framework').value
    return framework.versions
  }

  constructor(private service: CardsService, private loading: LoaderService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      dateOfBirth: new FormControl({ value: null, disabled: true }, Validators.required),
      framework: new FormControl(null, Validators.required),
      frameworkVersion: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email], EmailAsyncValidatorService.validate(this.service)),
      hobby: new FormArray([new FormGroup({
        name: new FormControl(null, Validators.required),
        duration: new FormControl(null, Validators.required)
      })])
    })
  }

  submit() {
    if (this.form.invalid) {
      return null
    }
    this.loading.open()

    const employer: IEmployer = {
      ...this.form.value,
      framework: this.form.value.framework.name
    }

    this.service.create(employer)
      .subscribe(() => {
        this.addEmployer.emit(employer)
        this.form.reset()
        this.loading.close()
      }, () => {
        this.loading.close()
      })
  }

  addHobby() {
    const hobby: FormGroup = new FormGroup({
      name: new FormControl(null, Validators.required),
      duration: new FormControl(null, Validators.required)
    })
    const arr: FormArray = <FormArray>this.form.get('hobby')
    arr.push(hobby)
  }

  removeHobby(id: number) {
    const arr = <FormArray>this.form.get('hobby')
    arr.removeAt(id)
  }
}
