import { Injectable } from "@angular/core";
import { AbstractControl, AsyncValidatorFn, ValidationErrors } from "@angular/forms";
import { Observable } from "rxjs";
import { CardsService } from "../card/cards.service";

@Injectable({ providedIn: "root" })

export class EmailAsyncValidatorService {
  static validate(service: CardsService):AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return service.validEmail(control.value)
    }
  }
}
