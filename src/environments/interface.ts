export interface IEnvironment {
  API_KEY: string,
  DB_URL: string,
  production: boolean
}
